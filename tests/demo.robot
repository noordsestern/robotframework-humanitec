*** Settings ***
Library         ../Humanitec/Humanitec.py
Resource        resources/score.resource

Test Tags       demo


*** Variables ***
${app_id}       score-zeebe
${DELTA}        ${EMPTY}


*** Test Cases ***
Create Delta With 2 Score Files and Deploy
    Create New Delta
    FOR    ${score_file}    IN    ${CURDIR}/score/zeebe-broker.yaml    ${CURDIR}/score/zeebe-monitor.yaml
        Create Delta With Score and Upload    score_file=${score_file}    app_id=${app_id}    delta_id=${DELTA}[id]
    END
    Deploy Complete Delta


*** Keywords ***
Create New Delta
    ${delta}    Create Delta    app_id=${app_id}
    Set Test Variable    ${delta}

Deploy Complete Delta
    Deploy Delta    app_id=${app_id}    delta_id=${delta}[id]
