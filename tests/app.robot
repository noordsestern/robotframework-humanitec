*** Settings ***
Resource        resources/common.resource

Test Tags       app    regression


*** Test Cases ***
Get Apps
    [Tags]    swagger-client
    ${apps}    Get All Apps
    Log To Console    ${apps}
