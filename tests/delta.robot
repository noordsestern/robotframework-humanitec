*** Settings ***
Resource        resources/common.resource

Test Tags       delta    regression


*** Test Cases ***
Get Deltas
    ${deltas}    Get Deltas For Application    app_id=score-zeebe
    Log To Console    ${deltas}
